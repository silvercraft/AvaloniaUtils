﻿using System;
using System.IO;
using System.Text.Json;
using System.Text.Json.Nodes;
using System.Xml;

namespace SilverCraft.AvaloniaUtils
{
    public interface IEnvBackend
    {
        public T? GetEnv<T>(string varName) where T : class;
        public E? GetEnum<E>(string varName) where E : struct;

        public bool? GetBool(string varName);
        public byte? GetByte(string varName);
        public decimal? GetDecimal(string varName);
        public float? GetSingle(string varName);
        public string? GetString(string varName);
        public int? GetInt(string varName);


        public void SetEnv<T>(string varName, T? value) where T : class;
        public void SetEnum<E>(string varName, E? value) where E : struct;

        public void SetBool(string varName, bool? value);
        public void SetByte(string varName, byte? value);
        public void SetDecimal(string varName, decimal? value);
        public void SetSingle(string varName, float? value);
        public void SetString(string varName, string? value);
        public void SetInt(string varName, int? value);
    }

    public class ModernDotFileBackend : IEnvBackend
    {
        readonly string FileLoc;
        private FileSystemWatcher _watcher;

        public ModernDotFileBackend(string loc)
        {
            FileLoc = loc;
            var dir = Path.GetDirectoryName(loc);
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }

            _watcher = new()
            {
                Filter = Path.GetFileName(loc),
                Path = dir,
                NotifyFilter = NotifyFilters.LastWrite,
                IncludeSubdirectories = false
            };
            _watcher.Changed += WatcherOnChanged;
            _watcher.EnableRaisingEvents = true;
        }

        private void WatcherOnChanged(object sender, FileSystemEventArgs e)
        {
            WindowExtensions.OnStyleChange.Invoke(this,
                new(GetString("SAPColor"),
                    WindowExtensions.GetTransparencyLevelsFromString(((IEnvBackend)this).GetString("SAPTransparency")),
                    GetString("SAPFontFamily")));
        }

        public JsonElement? GetJsonElement(string EnvvarName)
        {
            if (!File.Exists(FileLoc)) return null;
            var jsonData = JsonSerializer.Deserialize<JsonElement>(File.ReadAllText(FileLoc));
            if (jsonData.TryGetProperty(EnvvarName, out var val))
            {
                return val;
            }

            return null;
        }

        public bool? GetBool(string varName)
        {
            return GetJsonElement(varName)?.GetBoolean();
        }

        public byte? GetByte(string varName)
        {
            return GetJsonElement(varName)?.GetByte();
        }

        public decimal? GetDecimal(string varName)
        {
            return GetJsonElement(varName)?.GetDecimal();
        }

        public T? GetEnv<T>(string varName) where T : class
        {
            if (!File.Exists(FileLoc)) return null;
            var jsonData = JsonSerializer.Deserialize<JsonElement>(File.ReadAllText(FileLoc));
            return jsonData.TryGetProperty(varName, out var val) ? val.Deserialize<T>() : null;
        }

        public int? GetInt(string varName)
        {
            return GetJsonElement(varName)?.GetInt32();
        }

        public float? GetSingle(string varName)
        {
            return GetJsonElement(varName)?.GetSingle();
        }

        public string? GetString(string varName)
        {
            return GetJsonElement(varName)?.GetString();
        }

        public void SetBool(string varName, bool? value)
        {
            var jsonData = File.Exists(FileLoc)
                ? JsonSerializer.Deserialize<JsonObject>(File.ReadAllText(FileLoc))
                : new();
            if (jsonData.ContainsKey(varName))
            {
                jsonData[varName] = value;
            }
            else
            {
                jsonData.Add(new(varName, value));
            }

            File.WriteAllText(FileLoc, JsonSerializer.Serialize(jsonData));
        }

        public void SetByte(string varName, byte? value)
        {
            var jsonData = File.Exists(FileLoc)
                ? JsonSerializer.Deserialize<JsonObject>(File.ReadAllText(FileLoc))
                : new();

            if (jsonData.ContainsKey(varName))
            {
                jsonData[varName] = value;
            }
            else
            {
                jsonData.Add(new(varName, value));
            }

            File.WriteAllText(FileLoc, JsonSerializer.Serialize(jsonData));
        }

        public void SetDecimal(string varName, decimal? value)
        {
            var jsonData = File.Exists(FileLoc)
                ? JsonSerializer.Deserialize<JsonObject>(File.ReadAllText(FileLoc))
                : new();
            if (jsonData.ContainsKey(varName))
            {
                jsonData[varName] = value;
            }
            else
            {
                jsonData.Add(new(varName, value));
            }

            File.WriteAllText(FileLoc, JsonSerializer.Serialize(jsonData));
        }

        public void SetEnv<T>(string varName, T? value) where T : class
        {
            var jsonData = File.Exists(FileLoc)
                ? JsonSerializer.Deserialize<JsonObject>(File.ReadAllText(FileLoc))
                : new();
            if (jsonData.ContainsKey(varName))
            {
                jsonData[varName] = JsonSerializer.SerializeToNode(value);
            }
            else
            {
                jsonData.Add(new(varName, JsonSerializer.SerializeToNode(value)));
            }

            File.WriteAllText(FileLoc, JsonSerializer.Serialize(jsonData));
        }

        public void SetInt(string varName, int? value)
        {
            var jsonData = File.Exists(FileLoc)
                ? JsonSerializer.Deserialize<JsonObject>(File.ReadAllText(FileLoc))
                : new();
            if (jsonData.ContainsKey(varName))
            {
                jsonData[varName] = JsonSerializer.SerializeToNode(value);
            }
            else
            {
                jsonData.Add(new(varName, JsonSerializer.SerializeToNode(value)));
            }

            File.WriteAllText(FileLoc, JsonSerializer.Serialize(jsonData));
        }

        public void SetSingle(string varName, float? value)
        {
            var jsonData = File.Exists(FileLoc)
                ? JsonSerializer.Deserialize<JsonObject>(File.ReadAllText(FileLoc))
                : new();
            if (jsonData.ContainsKey(varName))
            {
                jsonData[varName] = JsonSerializer.SerializeToNode(value);
            }
            else
            {
                jsonData.Add(new(varName, JsonSerializer.SerializeToNode(value)));
            }

            File.WriteAllText(FileLoc, JsonSerializer.Serialize(jsonData));
        }

        public void SetString(string varName, string? value)
        {
            SetEnv(varName, value);
        }

        E? IEnvBackend.GetEnum<E>(string varName)
        {
            if (!File.Exists(FileLoc)) return null;
            var jsonData = JsonSerializer.Deserialize<JsonElement>(File.ReadAllText(FileLoc));
            if (jsonData.TryGetProperty(varName, out var val))
            {
                return Enum.Parse<E>(val.GetString());
            }

            return null;
        }

        void IEnvBackend.SetEnum<E>(string varName, E? value)
        {
            var jsonData = File.Exists(FileLoc)
                ? JsonSerializer.Deserialize<JsonObject>(File.ReadAllText(FileLoc))
                : new();
            if (jsonData.ContainsKey(varName))
            {
                jsonData[varName] = JsonSerializer.SerializeToNode(Enum.GetName(typeof(E), value));
            }
            else
            {
                jsonData.Add(new(varName, Enum.GetName(typeof(E), value)));
            }

            File.WriteAllText(FileLoc, JsonSerializer.Serialize(jsonData));
        }
    }

    public class LegacyEnviromentBackend : IEnvBackend
    {
        public T? GetEnv<T>(string varName) where T : class
        {
            if (typeof(T).IsAssignableFrom(typeof(string)))
            {
                return GetStringEnv(varName) as T;
            }

            if (typeof(T).IsAssignableFrom(typeof(bool)))
            {
                return XmlConvert.ToBoolean(GetStringEnv(varName)) as T;
            }

            if (typeof(T).IsAssignableFrom(typeof(byte)))
            {
                return XmlConvert.ToByte(GetStringEnv(varName)) as T;
            }

            if (typeof(T).IsAssignableFrom(typeof(decimal)))
            {
                return XmlConvert.ToDecimal(GetStringEnv(varName)) as T;
            }

            if (typeof(T).IsAssignableFrom(typeof(double)))
            {
                return XmlConvert.ToDouble(GetStringEnv(varName)) as T;
            }

            if (typeof(T).IsAssignableFrom(typeof(float)))
            {
                return XmlConvert.ToSingle(GetStringEnv(varName)) as T;
            }

            throw new NotImplementedException();
        }

        public string? GetStringEnv(string EnvvarName)
        {
            if (Environment.OSVersion.Platform == PlatformID.Win32NT)
            {
                return Environment.GetEnvironmentVariable(EnvvarName, EnvironmentVariableTarget.User);
            }

            return Environment.GetEnvironmentVariable(EnvvarName);
        }


        public void SetStringEnv(string EnvvarName, string? Value)
        {
            if (Environment.OSVersion.Platform == PlatformID.Win32NT)
            {
                Environment.SetEnvironmentVariable(EnvvarName, Value, EnvironmentVariableTarget.User);
            }
            else
            {
                Environment.SetEnvironmentVariable(EnvvarName, Value);
            }
        }

        public void SetEnv<T>(string varName, T? value) where T : class
        {
            if (typeof(T).IsAssignableFrom(typeof(string)))
            {
                SetStringEnv(varName, value as string);
                return;
            }

            if (typeof(T).IsAssignableFrom(typeof(bool)))
            {
                SetStringEnv(varName, XmlConvert.ToString((bool)(value as bool?)));
                return;
            }

            if (typeof(T).IsAssignableFrom(typeof(byte)))
            {
                SetStringEnv(varName, XmlConvert.ToString((byte)(value as byte?)));
                return;
            }

            if (typeof(T).IsAssignableFrom(typeof(decimal)))
            {
                SetStringEnv(varName, XmlConvert.ToString((decimal)(value as decimal?)));
                return;
            }

            if (typeof(T).IsAssignableFrom(typeof(double)))
            {
                SetStringEnv(varName, XmlConvert.ToString((double)(value as double?)));
                return;
            }

            if (typeof(T).IsAssignableFrom(typeof(float)))
            {
                SetStringEnv(varName, XmlConvert.ToString((float)(value as float?)));
                return;
            }

            throw new NotImplementedException();
        }

        public bool? GetBool(string varName)
        {
            return XmlConvert.ToBoolean(GetStringEnv(varName));
        }

        public byte? GetByte(string varName)
        {
            return XmlConvert.ToByte(GetStringEnv(varName));
        }

        public decimal? GetDecimal(string varName)
        {
            return XmlConvert.ToDecimal(GetStringEnv(varName));
        }

        public float? GetSingle(string varName)
        {
            return XmlConvert.ToSingle(GetStringEnv(varName));
        }

        public string? GetString(string varName)
        {
            return GetStringEnv(varName);
        }

        public void SetBool(string varName, bool? value)
        {
            SetStringEnv(varName, XmlConvert.ToString((bool)value));
        }

        public void SetByte(string varName, byte? value)
        {
            SetStringEnv(varName, XmlConvert.ToString((byte)value));
        }

        public void SetDecimal(string varName, decimal? value)
        {
            SetStringEnv(varName, XmlConvert.ToString((decimal)value));
        }

        public void SetSingle(string varName, float? value)
        {
            SetStringEnv(varName, XmlConvert.ToString((float)value));
        }

        public void SetString(string varName, string? value)
        {
            SetStringEnv(varName, value);
        }

        public int? GetInt(string varName)
        {
            return XmlConvert.ToInt32(GetStringEnv(varName));
        }

        public void SetInt(string varName, int? value)
        {
            SetStringEnv(varName, XmlConvert.ToString((int)value));
        }

        public E? GetEnum<E>(string varName) where E : struct
        {
            return Enum.Parse<E>(GetStringEnv(varName));
        }

        public void SetEnum<E>(string varName, E? value) where E : struct
        {
            SetStringEnv(varName, Enum.GetName(typeof(E), value));
        }
    }
}