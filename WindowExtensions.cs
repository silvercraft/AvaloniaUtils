﻿using Avalonia.Controls;
using Avalonia.Media;
using Avalonia.Threading;
using System;
using System.IO;
using System.Linq;

namespace SilverCraft.AvaloniaUtils
{
    public record StylingChangeData(string? SAPColor, WindowTransparencyLevel[]? SAPTransparency, string? fontFamily);

    public static class WindowExtensions
    {
        public static IEnvBackend envBackend = new ModernDotFileBackend(Path.Combine(
            Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "SilverCraftAvaloniav1Shared",
            "dotfile.json"));

        public static EventHandler<StylingChangeData> OnStyleChange;
        static bool DisSAPTransparency => envBackend.GetBool("DisableSAPTransparency") == true;

        public static Color ReadColor(this string varname, Color? def = null)
        {
            return ParseColor(envBackend.GetString(varname)) ?? def ?? Colors.Coral;
        }

        public static Color? ParseColor(string? value)
        {
            if (value is null) return null;
            if (Color.TryParse(value, out var c)) return c;
            if (Enum.TryParse(value, out KnownColor kc))
            {
                return kc.ToColor();
            }

            return null;
        }

        public static IBrush ParseBackground(this string? color, IBrush? def = null)
        {
            if (color == null) return def ?? new SolidColorBrush(Colors.Coral, DisSAPTransparency ? 1 : 0.3);
            if (color.Contains(','))
            {
                var colors = color.Split(',');
                var perpart = 1d / colors.Length;
                double alreadygiven = 0;

                var gradient = new LinearGradientBrush();

                foreach (var c in colors)
                {
                    var cc = ParseColor(c);
                    if (cc == null) continue;
                    gradient.GradientStops.Add(new((Color)cc, alreadygiven));
                    alreadygiven += perpart;
                }

                return gradient;
            }

            var co = ParseColor(color);
            return co == null
                ? (def ?? new SolidColorBrush(Colors.Coral, DisSAPTransparency ? 1 : 0.3))
                : new SolidColorBrush((Color)co, envBackend.GetBool("DisableSAPTransparency") == true ? 1 : 0.3);
        }

        public static Color ToColor(this KnownColor kc) => Color.FromUInt32((uint)kc);

        public static WindowTransparencyLevel GetTransparencyLevelFromString(string s) =>
            s switch
            {
                "AcrylicBlur" => WindowTransparencyLevel.AcrylicBlur,
                "Transparent" => WindowTransparencyLevel.Transparent,
                "Mica" => WindowTransparencyLevel.Mica,
                "Blur" => WindowTransparencyLevel.Blur,
                "None" => WindowTransparencyLevel.None,
                _ => WindowTransparencyLevel.AcrylicBlur
            };

        public static WindowTransparencyLevel[] GetTransparencyLevelsFromString(string s)
        {
            return s.Split(',').Select(GetTransparencyLevelFromString).ToArray();
        }
        public static FontFamily ReadEnvFontFamilyParseOrDefault(string? envName, FontFamily? def = null) => FontFamilyParseOrDefault(envBackend.GetString(envName),def);
        public static FontFamily ReadEnvFontFamilyParseOrDefault(string? envName, string? def) => FontFamilyParseOrDefault(envBackend.GetString(envName),def);
        public static FontFamily FontFamilyParseOrDefault(string? fontFamilyName, string? def) => fontFamilyName == null ? (def??FontFamily.Default) : FontFamily.Parse(fontFamilyName);

        public static FontFamily FontFamilyParseOrDefault(string? fontFamilyName, FontFamily? def = null) => fontFamilyName == null ? (def??FontFamily.Default) : FontFamily.Parse(fontFamilyName);
       /// <summary>
       /// Changes the window's appearance based on settings saved in the <see cref="envBackend"/>
       /// </summary>
       /// <param name="window">the window instance to change and or subscribe to changes</param>
       /// <param name="register">whether to subscribe the window to changes</param>
       /// <param name="defBackground">the default background if the user hasn't specified one</param>
       /// <param name="defFontFamily">the default font family if the user hasn't specified one</param>
       /// <param name="options">the subscription options</param>
       /// <param name="followOptionsOnCall">whether or not to follow the subscription options on first set</param>
        public static void DoAfterInitTasks(this Window window, bool register, IBrush? defBackground = null, string? defFontFamily =null,
            SubscriptionOptions options = null, bool followOptionsOnCall=true)
        {
            options ??= SubscriptionOptions.Default;
            if (!followOptionsOnCall || options.SetTransparency)
            {
                window.TransparencyLevelHint =
                    GetTransparencyLevelsFromString(envBackend.GetString("SAPTransparency") ?? "Mica,AcrylicBlur,None");
            }

            if (!followOptionsOnCall || options.SetBackground)
            {
                window.Background = ParseBackground(envBackend.GetString("SAPColor"), def: defBackground);
            }

            if (!followOptionsOnCall || options.SetFontFamily)
            {
                window.FontFamily = ReadEnvFontFamilyParseOrDefault("SAPFontFamily");
            }

            if (!register) return;
            EventHandler<StylingChangeData> x = (_, y) =>
            {
                if (y != null)
                {
                    if (options.SetTransparency && y.SAPTransparency != null)
                    {
                        Dispatcher.UIThread.InvokeAsync(() => window.TransparencyLevelHint = y.SAPTransparency);
                    }
                    if (options.SetBackground && y.SAPColor != null)
                    {
                        Dispatcher.UIThread.InvokeAsync(() => window.Background = ParseBackground(y.SAPColor, defBackground));
                    }
                    if (options.SetFontFamily && y.fontFamily != null)
                    {
                        Dispatcher.UIThread.InvokeAsync(() => window.FontFamily = FontFamilyParseOrDefault(y.fontFamily));
                    }
                }
                else
                {
                    Dispatcher.UIThread.InvokeAsync(() => window.DoAfterInitTasks(false, defBackground: defBackground));
                }
            };
            OnStyleChange += x;
            window.Closing += (_, _) =>
            {
                if (OnStyleChange != null)
                {
                    OnStyleChange -= x;
                }
            };
        }
    }

    /// <summary>
    /// Options for whether the transparency, background and font family should be changed if a change is detected
    /// </summary>
    public class SubscriptionOptions
    {
        public static readonly SubscriptionOptions Default = new();
        public bool SetTransparency { get; set; } = true;
        public bool SetBackground { get; set; } = true;
        public bool SetFontFamily { get; set; } = true;
    }
}